import numpy as np
import random
from sklearn import cluster, metrics


def generate_clusterers(X):
    """
    Defines a list of clustering algorithms and corresponding hyperparameters.
    """

    dbscan = dict()
    dbscan['clusterer'] = cluster.DBSCAN
    pairwise_distances = metrics.pairwise_distances(X)
    min_pairwise_dist = np.amin(pairwise_distances[np.nonzero(pairwise_distances)])
    max_pairwise_dist = np.amax(pairwise_distances)
    dbscan['params'] = [{'eps': np.linspace(min_pairwise_dist, max_pairwise_dist,20), 'min_samples': range(2,21)}]

    spectral = dict()
    spectral['clusterer'] = cluster.SpectralClustering
    spectral['params'] = [{'n_clusters': range(2,11), 'n_neighbors': range(2,21), 'affinity': ['nearest_neighbors']},
                          {'n_clusters': range(2,11), 'gamma': np.linspace(0.01,5.0,20), 'affinity': ['rbf']}]

    kmeans = dict()
    kmeans['clusterer'] = cluster.KMeans
    kmeans['params'] = [{'n_clusters' : range(2,11), 'random_state' : [random.randint(0,1000000) for _ in range(20)], 'init' : ['random']}]

    return [dbscan,spectral,kmeans]