from sklearn import grid_search
import random
import numpy as np
import itertools
import collections
import pickle
import sys


class COBS:

    def __init__(self):
        self.ml = []
        self.cl = []
        self.clusterings = None

    def load_clusterings_from_file(self, fn):
        with open(fn,'rb') as f:
            self.clusterings = pickle.load(f)

    def generate_clusterings(self, X, clusterers, output_path):
        self.clusterings = []
        for clusterer in clusterers:
            for param_setting in grid_search.ParameterGrid(clusterer['params']):
                sklearn_clusterer = clusterer["clusterer"](**param_setting)
                sklearn_clusterer.fit(X)
                pred = sklearn_clusterer.labels_.astype(np.int)
                # for each clustering we store
                #       (1) the actual clustering
                #       (2) the algorithm that generated it
                #       (3) the parameter settings that were used
                #       (4) the random state
                self.clusterings.append((pred,clusterer['clusterer'],param_setting,np.random.get_state()))

        if output_path:
            with open(output_path,'wb') as f:
                pickle.dump(self.clusterings,f)

    def load_constraints_from_file(self,fn):
        with open(fn) as file:
            for line in file:
                if int(line.split(',')[2]) == 1:
                    self.ml.append(sorted([int(line.split(',')[0]),int(line.split(',')[1])]))
                elif int(line.split(',')[2]) == -1:
                    self.cl.append(sorted([int(line.split(',')[0]),int(line.split(',')[1])]))
                else:
                    raise Exception("Encountered unexpected line format while reading constraints from " + fn)

    def generate_constraints_from_labels(self, labels, n_constraints, do_not_sample = []):
        pairs = COBS._generate_pairs(len(labels),n_constraints, do_not_sample)
        self.ml = [p for p in pairs if labels[p[0]] == labels[p[1]]]
        self.cl = [p for p in pairs if labels[p[0]] != labels[p[1]]]

    def ask_constraints_from_user(self, n_instances, n_constraints):
        pairs = COBS._generate_pairs(n_instances,n_constraints)
        for pair in pairs:
            if _query_yes_no("Should the following instances be in the same cluster?  " + str(pair[0]) + " and " + str(pair[1])):
                self.ml.append(pair)
            else:
                self.cl.append(pair)

    def sort_algorithms_with_scores(self, scores):
         normalized_scores = [float(i) for i in scores] / scores.max(axis=0)
         sorted_indices = [i[0] for i in sorted(enumerate(scores), reverse=True, key=lambda x:x[1])]
         return [(self.clusterings[i][1], self.clusterings[i][2], normalized_scores[i]) for i in sorted_indices]

    def run_random(self, all_sorted=False):
         random.shuffle(self.clusterings)
         ml_scores = [sum([c[ml_c[0]] == c[ml_c[1]] for ml_c in self.ml]) for c,_,_,_ in self.clusterings]
         cl_scores = [sum([c[cl_c[0]] != c[cl_c[1]] for cl_c in self.cl]) for c,_,_,_ in self.clusterings]
         total_scores = np.array([sum(x) for x in zip(ml_scores, cl_scores)])
         if all_sorted:
             return self.sort_algorithms_with_scores(total_scores)
         return self.clusterings[np.argmax(total_scores)]

    def run_active(self, budget, sample_size, training_set=None, labels=None, all_sorted=False):
        random.shuffle(self.clusterings)

        if not training_set:
            training_set = range(len(self.clusterings[0][0]))
        all_pairs = list(itertools.product(training_set,training_set))
        random.shuffle(all_pairs)
        pairs = all_pairs[:sample_size]

        weights = np.ones((len(self.clusterings),1)) / len(self.clusterings)

        self.ml = []
        self.cl = []

        while budget > 0:
            i,j = COBS._get_best_pair(self.clusterings, sample_size, weights,pairs)
            pairs.remove((i,j))

            if labels is None:
                same_cluster = _query_yes_no(
                    "Should the following instances be in the same cluster?  " + str(i) + " and " + str(j))
            else:
                same_cluster = labels[i] == labels[j]

            for c_cntr, (c,_,_,_) in enumerate(self.clusterings):

                if c[i] == c[j] and same_cluster:
                    weights[c_cntr] *= 2.0
                elif c[i] != c[j] and not same_cluster:
                    weights[c_cntr] *= 2.0
                else:
                    weights[c_cntr] /= 2.0

            if same_cluster:
                self.ml.append((i,j))
            else:
                self.cl.append((i,j))

            budget -= 1
            weights = weights / np.sum(weights)

        if all_sorted:
            return self.sort_algorithms_with_scores(weights)
        return self.clusterings[np.argmax(weights)]

    @staticmethod
    def _get_best_pair(clusterings,sample_size,weights,pairs):
        should_be_different = collections.defaultdict(int)
        should_be_same = collections.defaultdict(int)

        cur_best_value = np.float("inf")
        cur_best_c = None

        for pair in pairs:
            for c_cntr, (c,_,_,_) in enumerate(clusterings):
                if c[pair[0]] == c[pair[1]]:
                    should_be_same[pair] += weights[c_cntr]
                else:
                    should_be_different[pair] += weights[c_cntr]

            if abs(should_be_different[pair] - should_be_same[pair]) < cur_best_value:
                cur_best_value = abs(should_be_different[pair] - should_be_same[pair])
                cur_best_c = pair

        return cur_best_c

    @staticmethod
    def _generate_pairs(n_instances, n_constraints, do_not_sample = []):
        to_sample_from = [x for x in range(n_instances) if x not in do_not_sample]
        return [sorted(random.sample(to_sample_from, 2)) for _ in range(n_constraints)]


def _query_yes_no(question, default="yes"):
    """Ask a yes/no question via raw_input() and return their answer.

    "question" is a string that is presented to the user.
    "default" is the presumed answer if the user just hits <Enter>.
        It must be "yes" (the default), "no" or None (meaning
        an answer is required of the user).

    The "answer" return value is True for "yes" or False for "no".

    Taken from: http://code.activestate.com/recipes/577058/
    """
    valid = {"yes": True, "y": True, "ye": True,
             "no": False, "n": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)
        choice = raw_input().lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' "
                             "(or 'y' or 'n').\n")
