import argparse
import json
import os
import numpy as np
import clusterers
import cobs
from sklearn import cross_validation, metrics


def read_folds(folds_dir, n_folds):
    folds = []
    for i in range(n_folds):
        with open(folds_dir + "/" + str(i) + ".fold") as fold_file:
            data = json.load(fold_file)
            folds.append((data["train"],data["test"]))
    return folds


def create_folds(output_dir,n_folds):
    os.makedirs(output_dir + "/folds")

    skf = cross_validation.StratifiedKFold(labels, n_folds=n_folds, shuffle=True)
    for idx, (train_index, test_index) in enumerate(skf):
        to_write = dict()
        to_write["train"] = train_index.tolist()
        to_write["test"] = test_index.tolist()

        f = open(output_dir + "/folds/" + str(idx) + ".fold", 'w+')
        f.write(json.dumps(to_write))
        f.close()

parser = argparse.ArgumentParser()
parser.add_argument("dataset", help="the dataset for which cross-validation is performed")
parser.add_argument("output", help="directory in which all temporary files will be stored")
parser.add_argument("-nc", "--num_constraints",
                    help="the number of constraints to generate from the given cluster labels", type=int,
                    default=50)
parser.add_argument("-nf", "--num_folds",
                    help="the number of folds to use in cross-validation", type=int,
                    default=10)
parser.add_argument("-lf", "--load_folds", help="file containing generated cross-validation folds (from a previous run)")
parser.add_argument("-lc", "--load_clusterings", help="file containing generated clusterings (from a previous run)")
parser.add_argument("-a", "--active", help="should be True for active constraint selection", action="store_true")
parser.add_argument("-s", "--sample_size",
                    help="size of sample from which constraints will be selected in active strategy", type=int,
                    default=200)

args = parser.parse_args()

with open(args.dataset) as file:
    X = np.asarray([[float(digit) for digit in line.split(',')] for line in file])

c = cobs.COBS()

## load the data
labels = X[:, -1]
X = X[:, :-1]

## load the folds, or generate them if necessary
folds_dir = None
if not args.load_folds:
    create_folds(args.output, args.num_folds)
    folds_dir = args.output + "/folds"
else:
    folds_dir = args.load_folds
folds = read_folds(folds_dir, args.num_folds)

## load the clusterings, or generate them if necessary
clusterings_file = None
if not args.load_clusterings:
    c.generate_clusterings(X, clusterers.generate_clusterers(X), args.output + "/clusterings")
else:
    c.load_clusterings_from_file(args.load_clusterings)

## perform the cross-validation and print the average test set ARI
test_aris = []
for train, test in folds:
     if not args.active:
         c.generate_constraints_from_labels(labels,args.num_constraints,test)
         clustering = c.run_random()
     else:
        clustering = c.run_active(args.num_constraints, args.sample_size,train,labels)

     evaluation_set = [labels[i] for i in test]
     to_evaluate = [clustering[0][i] for i in test]
     test_aris.append(metrics.adjusted_rand_score(to_evaluate, evaluation_set))


print "The mean test set ARI over " + str(args.num_folds) + " folds: " + str(np.mean(test_aris))
